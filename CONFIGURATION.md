This document explains how to configure sshtargate.

# Configuration

Specify the path to a configuration file with ```--config``` or create a file
at the default path of ```~/.config/sshtargate/config.yaml```.

Define one or more **portals** by name/label with the following options:

- **command** - Command to execute
- **host** - One or more addresses to listen for connections

# Example config.yaml

```
portals:
  date and time:
    command: date
    host:
      - localhost:19001
  uname:
    command: uname -a
    host:
      - localhost:19002
  process list:
    command: ps -aux
    host:
      - localhost:19003
      - :19004 # Expose port 19004 to the internet
  system monitor:
    command: htop
    host:
      - localhost:19005
```
