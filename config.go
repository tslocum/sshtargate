package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

type portalConfig struct {
	Command string
	Host    []string `yaml:",flow"`
}

type appConfig struct {
	Portals map[string]*portalConfig
}

var config = &appConfig{}

func readConfig(configPath string) error {
	configData, err := ioutil.ReadFile(configPath)
	if err != nil {
		return fmt.Errorf("failed to read file: %s", err)
	}

	err = yaml.Unmarshal(configData, config)
	if err != nil {
		return fmt.Errorf("failed to parse file: %s", err)
	}

	if len(config.Portals) == 0 {
		log.Println("Warning: No portals are defined")
	}

	return nil
}
