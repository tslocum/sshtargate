module gitlab.com/tslocum/sshtargate

go 1.13

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be
	github.com/creack/pty v1.1.11
	github.com/gliderlabs/ssh v0.3.0
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
	golang.org/x/sys v0.0.0-20200615200032-f1bc736245b1 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
