# sshtargate
[![GoDoc](https://gitlab.com/tslocum/godoc-static/-/raw/master/badge.svg)](https://docs.rocketnine.space/gitlab.com/tslocum/sshtargate/portal)
[![CI status](https://gitlab.com/tslocum/sshtargate/badges/master/pipeline.svg)](https://gitlab.com/tslocum/sshtargate/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Host SSH portals to applications

## Demo

Connect to a sshtargate portal which executes the
[cview](https://gitlab.com/tslocum/cview) [presentation demo](https://gitlab.com/tslocum/cview/blob/master/demos/presentation/main.go):

```bash
ssh cview.rocketnine.space -p 20000
```

## Install

Choose one of the following methods:

### Download

[**Download sshtargate**](https://sshtargate.rocketnine.space/download/?sort=name&order=desc)

### Compile

```bash
go get gitlab.com/tslocum/sshtargate
```

## Configure

See [CONFIGURATION.md](https://gitlab.com/tslocum/sshtargate/blob/master/CONFIGURATION.md)

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/sshtargate/issues).
